#!/bin/bash

docker volume create --name postgres_data
docker volume create --name nexus_data
docker volume create --name jenkins_data
docker volume create --name portainer_data 

docker network create DEVOPS_NET --subnet 192.168.100.0/24 --gateway 192.168.100.1
 
docker run -d \
    --name postgresql \
    -e POSTGRES_PASSWORD=pass123 \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /custom/mount:/var/lib/postgresql/data \
    postgres

sysctl -w vm.max_map_count=524288
sysctl -w fs.file-max=131072
ulimit -n 131072
ulimit -u 8192
docker run -d --name sonarqube -p 9000:9000 -v sonarqube_data:/opt/sonarqube/data -v sonarqube_extensions:/opt/sonarqube/extensions -v sonarqube_logs:/opt/sonarqube/logs sonarqube 
docker run -d --name portainer -v portainer_data:/portainer_data portainer/portainer-ce 
docker run -d --name nexus -v nexus_data:/nexus_data sonatype/nexus3
docker run -d --name jenkins -v jenkins_data:/jenkins_data jenkins/jenkins
 
docker network connect DEVOPS_NET nexus 
docker network connect DEVOPS_NET portainer
docker network connect DEVOPS_NET jenkins
docker network connect DEVOPS_NET postgresql
docker network connect DEVOPS_NET sonarqube